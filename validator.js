function validSolution(board){

   let isBoardValid = true;
  let i,j,iStart,iEnd,jStart,jEnd;
  let sqArray = [];
  let numbers = [1,2,3,4,5,6,7,8,9];
  
  let sumRow = 0,rowSums = [],sumCol = 0,colSums = [];
  
  board.forEach((x,i,brd)=>{
    brd[i].forEach((y,j,row)=>{
      sumRow+=row[j];
      sumCol+=brd[j][i]
      if((j+1)%9==0){
        rowSums.push(sumRow);
        colSums.push(sumCol);
        sumRow=0; sumCol=0;
      }
    });
  });


function test9x9(iStart,iEnd,jStart,jEnd){
  for(i=iStart;i<iEnd;i++){
    for(j=jStart;j<jEnd;j++){
      sqArray.push(board[i][j]);
  }
}
 
  for(i=0;i<9;i++){
    if(sqArray.indexOf(numbers[i])<0){return false;}
  }
  sqArray = [];
  return true;
}


 function testEach9x9(){
   return test9x9(0,3,0,3)&&
        test9x9(0,3,3,6)&&
         test9x9(0,3,6,9)&&
         test9x9(3,6,0,3)&&
         test9x9(3,6,3,6)&&
         test9x9(3,6,6,9)&&
         test9x9(6,9,0,3)&&
         test9x9(6,9,3,6)&&
         test9x9(6,9,6,9);
 }
 
  return rowSums.every((x,i,arr)=>arr[0]==arr[i])&&
    colSums.every((x,i,arr)=>arr[0]==arr[i])&&testEach9x9();

}